package co.simplon.blog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.blog.entity.Comment;
import co.simplon.blog.repository.CommentRepository;
import jakarta.validation.Valid;

@CrossOrigin("*")
@RequestMapping("/api/comment")
@RestController 

public class CommentController {

    @Autowired 
    private CommentRepository repo;

    @GetMapping
    public List<Comment> all(){
        return repo.findAll();
    }

    @GetMapping("/{id}")
    public Comment onComment(@PathVariable int id){
        Comment comment = repo.findbyId(id);
        if (comment == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return comment;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Comment add(@RequestBody Comment comment) {
        repo.persist(comment);
        return comment;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        onComment(id); 
        repo.delete(id);
    }

    @PutMapping("/{id}")
    public Comment replace(@PathVariable int id, @Valid @RequestBody Comment comment) {
        onComment(id);
        comment.setId(id);
        repo.update(comment);
        return comment;
    }

}
