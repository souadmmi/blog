package co.simplon.blog.entity;

import java.time.LocalDateTime;

public class Article {

    private int id;
    private String title;
    private String content;
    private String picture;
    private LocalDateTime date;
    private int vue;
    private User user;
    private Categorie categorie;

    public Article() {
    }


    public Article(String title, String content, String picture, LocalDateTime date, int vue, User user,
            Categorie categorie) {
        this.title = title;
        this.content = content;
        this.picture = picture;
        this.date = date;
        this.vue = vue;
        this.user = user;
        this.categorie = categorie;
    }


    public Article(int id, String title, String content, String picture, LocalDateTime date, int vue, User user,
            Categorie categorie) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.picture = picture;
        this.date = date;
        this.vue = vue;
        this.user = user;
        this.categorie = categorie;
    }
    
    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public String getContent() {
        return content;
    }


    public void setContent(String content) {
        this.content = content;
    }


    public String getPicture() {
        return picture;
    }


    public void setPicture(String picture) {
        this.picture = picture;
    }


    public LocalDateTime getDate() {
        return date;
    }


    public void setDate(LocalDateTime date) {
        this.date = date;
    }


    public int getVue() {
        return vue;
    }


    public void setVue(int vue) {
        this.vue = vue;
    }


    public User getUser() {
        return user;
    }


    public void setUser(User user) {
        this.user = user;
    }


    public Categorie getCategorie() {
        return categorie;
    }


    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }


    
    
}
