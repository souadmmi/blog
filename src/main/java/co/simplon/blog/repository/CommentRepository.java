package co.simplon.blog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.blog.entity.Comment;

@Repository
public class CommentRepository {

    @Autowired
    private DataSource dataSource;


    public List<Comment> findAll(){
        List<Comment> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt= connection.prepareStatement("SELECT * FROM comment");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Comment comment = new Comment(
                    result.getInt("id"),
                    result.getString("content"),
                    result.getTimestamp("date").toLocalDateTime(),
                    result.getString("author")
                );
                list.add(comment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("repo error");
        }
        return list;
    }

    public List<Comment> findAllOfArticle(int id){
        List<Comment> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt= connection.prepareStatement("SELECT * FROM comment WHERE id_article=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Comment comment = new Comment(
                    result.getInt("id"),
                    result.getString("content"),
                    result.getTimestamp("date").toLocalDateTime(),
                    result.getString("author")
                );
                list.add(comment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("repo error");
        }
        return list;
    }

    public Comment findbyId(int id){
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM comment WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Comment(
                    result.getInt("id"),
                    result.getString("content"),
                    result.getTimestamp("date").toLocalDateTime(),
                    result.getString("author")
                );
                
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public boolean delete (int id){
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM comment WHERE id=?");
            stmt.setInt(1, id);
            if (stmt.executeUpdate()==1) {
                return true;
                
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public boolean persist(Comment comment){
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO comment (content , date, author) VALUES (?, ?, ?)");
            stmt.setString(1, comment.getContent());
            stmt.setTimestamp(2, Timestamp.valueOf( comment.getDate()));
            stmt.setString(3, comment.getAuthor());

            if(stmt.executeUpdate() == 1) {
                
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                comment.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(Comment comment){
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE comment SET content=?, date=?, author=?  WHERE id=?" );
            stmt.setString(1, comment.getContent());
            stmt.setTimestamp(2, Timestamp.valueOf( comment.getDate()));
            stmt.setString(3, comment.getAuthor());
            stmt.setInt(4, comment.getId());
            if(stmt.executeUpdate()==1){
                return true;
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }


}