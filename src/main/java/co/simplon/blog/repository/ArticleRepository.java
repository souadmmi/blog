package co.simplon.blog.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.blog.entity.Article;
import co.simplon.blog.entity.User;





    @Repository

public class ArticleRepository {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private CategorieRepository categorierepo;

    @Autowired
    private UserRepository userRepo;


    public List<Article> findAll(){
        List<Article> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt= connection.prepareStatement("SELECT * FROM article");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Article article = new Article(
                    result.getInt("id"),
                    result.getString("title"),
                    result.getString("content"),
                    result.getString("picture"),
                    result.getTimestamp("date").toLocalDateTime(),
                    result.getInt("vue"),
                    userRepo.findById(result.getInt("id_user")).get(),
                    categorierepo.findbyId(result.getInt("id_categorie"))
                );
                list.add(article);
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return list;
    }

    public List<Article> findAllOfCategorie(int id_categorie){
        List<Article> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt= connection.prepareStatement("SELECT * FROM article WHERE id_categorie=?");
            stmt.setInt(1, id_categorie);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                User user = new User();
                user.setId(result.getInt("id_user"));
                Article article = new Article(
                    result.getInt("id"),
                    result.getString("title"),
                    result.getString("content"),
                    result.getString("picture"),
                    result.getTimestamp("date").toLocalDateTime(),
                    result.getInt("vue"),
                    userRepo.findById(result.getInt("id_user")).get(),
                    categorierepo.findbyId(result.getInt("id_categorie"))
                );
                list.add(article);
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return list;
    }

    public List<Article> findThreeLatest(){
        List<Article> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt= connection.prepareStatement("SELECT * FROM article ORDER BY id DESC LIMIT 3");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Article article = new Article(
                    result.getInt("id"),
                    result.getString("title"),
                    result.getString("content"),
                    result.getString("picture"),
                    result.getTimestamp("date").toLocalDateTime(),
                    result.getInt("vue"),
                    userRepo.findById(result.getInt("id_user")).get(),
                    categorierepo.findbyId(result.getInt("id_categorie"))
                );
                list.add(article);
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return list;
    }

    public List<Article> findThreeLatestOfCategorie(int id_categorie){
        List<Article> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt= connection.prepareStatement("SELECT * FROM article WHERE id_categorie=? ORDER BY id DESC LIMIT 3");
            stmt.setInt(1, id_categorie);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                
                Article article = new Article(
                    result.getInt("id"),
                    result.getString("title"),
                    result.getString("content"),
                    result.getString("picture"),
                    result.getTimestamp("date").toLocalDateTime(),
                    result.getInt("vue"),
                    userRepo.findById(result.getInt("id_user")).get(),
                    categorierepo.findbyId(result.getInt("id_categorie"))
                );
                list.add(article);
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return list;
    }


    public Article findbyId(int id){
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                
                Article article = new Article(
                    result.getInt("id"),
                    result.getString("title"),
                    result.getString("content"),
                    result.getString("picture"),
                    result.getTimestamp("date").toLocalDateTime(),
                    result.getInt("vue"),
                    userRepo.findById(result.getInt("id_user")).get(),
                    categorierepo.findbyId(result.getInt("id_categorie"))
                    
                    
                );
                article.setCategorie(categorierepo.findbyId(result.getInt("id_categorie")));
                
                return article;
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return null;
    }

    public boolean delete (int id){
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM article WHERE id=?");
            stmt.setInt(1, id);
            if (stmt.executeUpdate()==1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return false;
    }

    public boolean persist(Article article){
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO article (title, content, picture, date, vue, id_user, id_categorie) VALUES (?,  ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, article.getTitle());
            stmt.setString(2, article.getContent());
            stmt.setString(3, article.getPicture());
            stmt.setTimestamp(4, Timestamp.valueOf( article.getDate()));
            stmt.setInt(5, article.getVue());
            stmt.setInt(6, article.getUser().getId());
            stmt.setInt(7, article.getCategorie().getId());

            if(stmt.executeUpdate() == 1) {
                
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                article.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error repo");
            throw new RuntimeException(e);
        }
        return false;
    }

    public boolean update(Article article){
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE article SET title=?, content=?, picture=?, date=?, vue=?, id_user=?, id_categorie=? WHERE id=?" );
            stmt.setString(1, article.getTitle());
            stmt.setString(2, article.getContent());
            stmt.setString(3, article.getPicture());
            stmt.setTimestamp(4, Timestamp.valueOf( article.getDate()));
            stmt.setInt(5, article.getVue());
            stmt.setInt(6, article.getUser().getId());
            stmt.setInt(7, article.getCategorie().getId());
            stmt.setInt(8, article.getId());
            if(stmt.executeUpdate()==1){
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return false;
    }


    public List<Article> findAllArticleOfOneUser(int id_user){
        List<Article> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt= connection.prepareStatement("SELECT * FROM article WHERE id_user=?");
            stmt.setInt(1, id_user);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Article article = new Article(
                    result.getInt("id"),
                    result.getString("title"),
                    result.getString("content"),
                    result.getString("picture"),
                    result.getTimestamp("date").toLocalDateTime(),
                    result.getInt("vue"),
                    userRepo.findById(result.getInt("id_user")).get(),
                    categorierepo.findbyId(result.getInt("id_categorie"))
                );
                list.add(article);
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return list;
    }

    public List<Article> findThreeLatestArticleOfOneUser(int id_user){
        List<Article> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt= connection.prepareStatement("SELECT * FROM article WHERE id_user=? ORDER BY id DESC LIMIT 3");
            stmt.setInt(1, id_user);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Article article = new Article(
                    result.getInt("id"),
                    result.getString("title"),
                    result.getString("content"),
                    result.getString("picture"),
                    result.getTimestamp("date").toLocalDateTime(),
                    result.getInt("vue"),
                    userRepo.findById(result.getInt("id_user")).get(),
                    categorierepo.findbyId(result.getInt("id_categorie"))
                );

                list.add(article);
            }
        } catch (SQLException e) {
            System.out.println("Repository Error");
            throw new RuntimeException(e);
        }
        return list;
    }


}
