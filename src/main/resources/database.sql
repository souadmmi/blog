-- Active: 1709545609475@@127.0.0.1@3306@blog



DROP TABLE IF EXISTS comment;
DROP TABLE IF  EXISTS article;
DROP TABLE IF EXISTS user;
DROP TABLE IF  EXISTS categorie;



CREATE TABLE categorie(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(200)
);

CREATE TABLE user(
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL,
    name VARCHAR(60),
    password VARCHAR(255) NOT NULL ,
    role VARCHAR(100) NOT NULL
    
);
CREATE TABLE article(
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(600),
    content VARCHAR(10000),
    picture VARCHAR(1500),   
    date DATETIME,
    vue INT,
    id_user INT, 
    Foreign Key (id_user) REFERENCES user(id) ON DELETE CASCADE,
    id_categorie INT,
    Foreign Key (id_categorie) REFERENCES categorie(id) ON DELETE CASCADE
);


CREATE TABLE comment(
    id INT PRIMARY KEY AUTO_INCREMENT,
    content VARCHAR(500),
    date DATETIME,
    id_article INT,
    Foreign Key (id_article) REFERENCES article(id) ON DELETE CASCADE
);
INSERT INTO categorie (name) VALUES 
("Forme & Bien-être"),
("Bien choisir ses aliments"),
("Recettes");
INSERT INTO user (email, name, password, role) VALUES
('admin@test.com', "adminame", '$2a$12$F3uxXF8HTLdnRuqL0y.mpu1g/V61XzV0fVJ6rEYEG1hb1PwpNaeiK', 'ROLE_ADMIN'),
('user@gmail.com', "username", '$2a$12$F3uxXF8HTLdnRuqL0y.mpu1g/V61XzV0fVJ6rEYEG1hb1PwpNaeiK', 'ROLE_USER');

INSERT INTO article (title, content, picture, date, vue, id_categorie, id_user) VALUES 
("Les 3 sensations alimentaires",  "Être à l’écoute de vos sensations … L’avez-vous déjà été ?
Sauriez-vous définir les 3 sensations alimentaires que votre corps vous transmet chaque jour ?
LA FAIM
Savez-vous pourquoi a-t-on faim, et comment se manifeste-t-elle ?
La faim est une sensation qui se déclenche lorsque votre taux de sucre moyen dans le sang diminue. C’est alors un besoin physiologique que de manger. Mais savez-vous de quelle manière votre corps se débrouille-t-il pour vous inciter à manger ? Avez-vous-déjà ressenti des gargouillis, des crampes stomacales, un creux au ventre, une baisse de régime, une perte de concentration ?
Ces derniers sont des signaux que vous envoie votre corps pour vous prévenir que vous avez FAIM. Dans ce cas précis, il sera très important d’écouter votre corps et de manger. Pour tout le reste, c’est de l’envie de manger... et c’est bien là toute la difficulté. L’envie de manger est souvent liée à une émotion ou un état (tristesse, colère, stress). Il est important dans ce moment, d’accueillir avec bienveillance cette envie de manger liée à cette émotion. MAIS ... Prenez votre temps lorsque vous mangez un aliment réconfort. Engloutir une ligne de chocolat en 2 minutes top chrono ne fera que vous culpabiliser et votre envie de manger sera toujours présente. Prenez le temps, 15-20 minutes pour manger votre aliment réconfort, et faites-le en pleine conscience et appréciez le plaisir que cela vous procure.
LE RASSASIEMENT
Le rassasiement c’est cette sensation qui vous dit que vous n’avez plus faim. A savoir qu’il y a deux types de rassasiement qui entre en jeu :
- le rassasiement mécanique,
- le rassasiement hormonal.
Le rassasiement mécanique, c’est lorsque votre estomac voit ses paroies distendues. Il est donc facile d’atteindre ce rassasiement. Mais un rassasiement mécanique seul, ne vous permettra pas de ne pas avoir faim plus tard. Le rassasiement hormonal lui, est dû à la libération d’hormones digestives. Celles-ci ne sont libérées qu’au bout de 20 minutes après le début du repas. Il sera donc très important de prendre le temps de manger si vous souhaitez obtenir un rassasiement digne de ce nom.
LA SATIETE
A ne pas confondre avec le rassasiement, la satiété c’est cette sensation de ne pas avoir faim entre 2 repas. Cette période peut être plus ou moins longue et dépend directement du repas précédent. 
La satiété est très utile, que ce soit pour une perte de poids, pour éviter les grignotages, ou tout simplement pour équilibrer son alimentation. 
Maintenant que vous connaissez les 3 sensations alimentaires, faites le test, avant de manger, essayer d’évaluer votre sensation de faim, tout en prenant conscience des signaux envoyés par votre corps lorsque l'heure du repas approche. Puis essayez de discerner la sensation de rassasiement qui prend place en fin de repas."
, "http://localhost:8080/pancake.png" ,
'2024-03-14T18:15:00',0, 1, 1),
("Comment bien choisir un jus de citron en bouteille",
"#1. Vérifier la teneur en… citron, bien sûr !
Première astuce pour choisir un jus de citron en bouteille : regardez la liste des ingrédients et notamment la teneur en citron. Cela semble évident mais on peut constater que tous les jus de citron ne contiennent pas la même quantité de citron ! Exemple : alors que certains jus de citron industriels contiennent 99,8% voire 100% de jus de citron, d’autres n’en contiennent que… 57% ! Vous imaginez donc la différence en terme de qualité nutritionnelle…
Mon conseil de diététicienne : Préférez les jus de citron en bouteille les plus riches en citron. La meilleure option reste de se tourner vers les jus de citron dit “100 % pur jus”, ce qui signifie que le jus en question ne contient aucun autre ingrédient.", "http://localhost:8080/citron.png",
'2024-03-14T18:15:00', 0, 2, 2),
("Les 6 meilleures applications pour faire du sport chez vous", "1/ Down Dog : l’application Yoga aux contenus variés
On commence avec une application de yoga très appréciée et disponible sur Apple et Android : l’application Down Dog (en français : “Chien tête en bas”). C’est personnellement mon application de yoga préférée puisque je l’utilise depuis 4 ans maintenant !
2/ Freeletics : l’application fitness qui ne nécessite aucun matériel
Avec cette application de sport, vous n’avez besoin que d’une chose : votre corps. Le principe de l’app Freeletics repose sur l’utilisation du poids de son corps pour s’entraîner à la maison. Elle est disponible sur Apple et Android, et est plutôt adaptée à ceux qui souhaitent faire du fitness à la maison de manière plus intensive.
3/ 7 Minutes Workout : l’application parfaite pour se lancer
Disponible sur Apple et Android, cette application vous révèle tout dans son nom : elle fait la promesse de vous tenir en forme avec seulement 7 minutes par jour. Les séances d’entraînement sont courtes mais généralement intenses, afin d’obtenir les meilleurs résultats selon vos objectifs fitness.
4/ Endomondo Sports Tracker : l’application de tracking par excellence
Voici maintenant une application de fitness qui vous permet de suivre vos entraînements et tracker vos progrès au fil des semaines. L’application Endomondo ne propose pas de contenu vidéo pour faire du sport, mais c’est l’une des applications les plus complètes pour enregistrer la progression dans votre activité physique.
Ses nombreuses fonctionnalités vous aident à tracker vos entraînements de vélo, de course à pied ou de fitness à la maison, avec une communauté de plusieurs millions d’utilisateurs pour partager conseils et encouragements. Il est possible de l’utiliser avec une montre connectée, et elle est disponible gratuitement sur Apple et Android.
5/ Nike Training Club : du yoga aux séances de fitness intenses
La marque Nike vous propose une application fitness complète avec un contenu gratuit très riche. L’appli NTC, Nike Training Club, met à disposition de ses utilisateurs plus de 200 sessions d’entraînement, allant du yoga doux aux séances de musculation poussées. Tous les niveaux sont accessibles et les cours sont variés.
Cette application de sport à la maison est gratuite, et les exercices se font avec ou sans matériel. Leur contenu vidéo ne cesse de se développer, avec pour objectif d’être l’une des applications de sport les plus complètes du marché. Elle est disponible sur Apple et Android.
6/ Daily Yoga : l’application qui combine yoga et méditation
Je termine cet article avec une dernière application de yoga. Le vrai plus de cette appli est qu’elle vous offre un contenu riche en cours de yoga (plus de 1 000 cours) mais également des séances de méditation. C’est un outil de remise en forme et de développement personnel, avec différents professeurs et tous les niveaux.
L’application Daily Yoga est gratuite, avec un contenu plus que satisfaisant en version gratuite. Elle propose également plusieurs abonnements au mois ou à l’année si vous voulez aller plus loin.
Vous avez maintenant le choix entre 6 applications de sport pour trouver celle qui vous convient. Ne soyez pas trop exigeant avec vous-même si vous vous lancez : prenez le temps de développer une routine sportive efficace et pratiquez un sport qui vous plait !
Avez-vous déjà testé une application de sport pour vous entraîner ? Dites-le-moi en commentaires !",
"http://localhost:8080/sportappli.png",
'2024-05-14T18:15:00', 0, 1, 2),
("5 façons de prévenir le blues hivernal",
"#1 Prendre l’air régulièrement
Le blues hivernal touche principalement les personne qui sortent le moins souvent de chez eux. Si l’hiver, l’accès à la lumière naturelle est réduit de par le faible niveau d’ensoleillement, il peut être aggravé davantage lorsque l’on travaille à la maison (“homeoffice”) ou lorsqu’on se déplace peu (seniors, personnes âgées ou sans emploi, etc).
#2 Faire une cure de vitamine D
Il est connu que la vitamine D exerce une influence sur la production de sérotonine et de dopamine, deux neurotransmetteurs impliqués dans les états dépressifs lorsqu’ils sont présents en quantités insuffisantes dans l’organisme.
#3 Essayer la luminothérapie
La luminothérapie (aussi appelée photothérapie) est une technique qui consiste à s’exposer à une lampe reproduisant la lumière naturelle. Cette médecine douce permet justement de limiter les risques de blues hivernal en palliant à la baisse drastique de luminosité spécifique de cette période de l’année.
#4 Faire du sport
Le sport fait partie des principales recommandations officielles pour lutter contre la dépression, c’est pourquoi il est évident qu’il fasse aussi partie des conseils de bases pour réduire le risque de blues en hiver. En effet, pratiquer une activité physique favorise la libération d’endorphines, les fameuses “hormones du plaisir”. Ces dernières contribuent à diminuer le stress, augmenter le bien-être et donc à réduire les risques de déprime saisonnière.
#5 S’accorder un week-end au soleil
A la mer, en montagne ou en forêt, partir en week-end au soleil permet de se changer l’esprit et de faire le plein de vitamine D, bonne pour le moral !
S’il est difficile de voyager loin en période de pandémie de coronavirus, partir en week-end dans les alentours, ou au moins en escapade journalière, est toujours utile pour s’échapper du quotidien et de recharger les batteries lorsqu’on n’a pas le moral dans l’assiette.",
"http://localhost:8080/blues.png", '2024-05-14T18:15:00', 0, 1, 1),
("La Battle des boissons végétales au soja : quelle boissons soja choisir ?",
"Les boissons végétales comme celles au soja fleurissent sur les étalages nous permettant de réduire notre consommation de produits laitiers et parallèlement d’insérer plus de produits d’origine végétale dans nos assiettes. Mais les boisson végétales ont pour inconvénient de nécessiter un processus de fabrication plus complexe qu’un simple lait de vache (qui a pour avantage d’être un produit brut non transformé). Bien choisir sa boisson végétale au soja, pour ne pas tomber dans le piège des produits ultra-transformés, est donc fondamental. Mais alors, quelle boisson soja choisir ?
Battle du jour : boisson soja Alpro vs. Provamel
Boisson au soja Alpro
Liste d’ingrédients : base de soja (eau, fèves de soja sans OGM décortiquées (8%)), sucre, correcteurs d’acidité (phosphates de potassium), carbonate de calcium, arôme, sel marin de table, stabilisant (gomme gellane), vitamines (B2, B12, D2).

Analyse de la liste d’ingrédients : 

❌contient des sucres ajoutés
❌contient des arômes de provenance non identifiée (et donc certainement des arômes synthétiques)
✅enrichi en vitamines – intéressant pour les personnes qui ne se supplémentent pas. Pour les personnes qui se supplémentent, attention aux hypervitaminoses (excès d’apports en vitamines).
❌contient des additifs, notamment des phosphates de potassium ou E340 (= un additif qui contient du phosphore et qui en excès peut contribuer à l’augmentation de la mortalité cardiovasculaire [1]).
❌pas bio
Boisson au soja Provamel
Liste d’ingrédients : base de SOJA (eau, fèves de SOJA décortiquées (9,8%))

Analyse de la liste d’ingrédients : 

✅sans sucre ajouté
✅composition courte et simple (seulement le nécessaire)
✅pas d’additifs ni autres ingrédients indésirables
✅bio

Verdict : Alpro ou Provamel, quelle boisson soja choisir ?
Vouloir réduire les produits laitiers, c’est bien. Mais les remplacer par des produits ultra-transformés, contenant des sucres ajoutés et des additifs, c’est pas top… C’est pourquoi quelque soit les alternatives végétales que vous choisissez (boisson soja, lait d’amande, boisson à l’avoine, etc.), prenez soin de bien les choisir :

bien choisir au supermarché
Choisissez autant que possible les boissons au soja “sans sucres ajoutés”. Gardez cependant en tête que la mention “sans sucres ajoutés” n’est pas le seul critère à prendre en compte.
Prenez le temps de lire la liste d’ingrédients. Choisissez les compositions les plus courtes et les plus simples possibles (sans additifs et sans arômes).
Je vous invite autant que possible à privilégier les boissons à base de soja issu d’Union Européenne (qui offrent la garantie d’être à base de soja sans OGM).
Les boissons au soja issu d’agriculture biologique (boisson au soja bio) sont une garantie supplémentaire au niveau de l’origine des fèves de soja : si votre budget vous y autorise, ne vous en privez pas !",
"http://localhost:8080/soja.png", '2024-03-14T18:15:00', 0, 2,2),
("Lecture d’étiquettes : Bien choisir son carré frais",
"Qui est fan de carrés frais ici ? :-) Dans ce nouvel épisode des Battle de DrBonneBouffe, je vous propose d’analyser la composition de deux marques de carrés frais natures, les Carrés frais Elle&Vire ainsi que les Carrés frais Carrefour qui affichent un prix bien inférieur à leur concurrent.

La battle des carrés frais : Carrefour vs. Elle & Vire, lequel choisir ?
Zoom sur les carrés frais Carrefour⁠
Liste d’ingrédients : fromage blanc 50% (crème de lait 25%, lait, ferments lactiques), lait écrémé reconstitué, beurre, eau, protéines de lait, sel de fonte : citrate de sodium, sel, correcteur d’acidité : acide citrique, vitamine A, vitamine B2, vitamine B12.

Analyse de la composition :

❌Composition longue
❌Ajout d’additifs (correcteurs d’acidités + citrate de sodium = permettant une texture homogène ⚠️: cet additif est associé à un risque de douleurs abdominales, de vomissements et de nausées à exposition élevée)⁠
❌Ajouts d’ingrédients à haut niveau de transformation (lait reconstitué, protéines de lait, 2 additifs)⁠
❌Ajout d’eau pour alléger le produit et réduire son prix de vente.⁠

Mon verdict de diététicienne ?
Les carrés frais nature Carrefour sont un produit est ultra-transformé, issu de multiples processus industriels qui détruisent le potentiel santé du produit. L’ajout de vitamines ne rend pas nécessairement l’aliment plus équilibré sur le plan nutritionnel dans la mesure où il nous incite à commencer un produit industriellement transformé. C’est un produit que je ne recommande pas à mes lecteurs et patients👎👎👎.⁠
Zoom sur les carrés frais Elle & Vire
Liste d’ingrédients : lait écrémé et crème, sel, ferments lactiques.

Analyse de la composition :

⁠✅Composition courte⁠
✅Seulement 4 ingrédients : du lait, de la crème, du sel et des ferments.⁠
✅Pas d’ajout de substances indésirables.⁠

Mon verdict de diététicienne ?
Les carrés frais Elle & Vire sont un produit peu transformé, avec une bonne composition. C’est un bon choix que je recommande à mes lecteurs et patients👍👍👍⁠",
"http://localhost:8080/carrefrais.png",
'2024-05-14T18:15:00',0, 2, 1),
("Galette des rois légère en sucre (recette facile⭐)",
"INGRÉDIENTS
2 pâtes feuilletées
2 œufs (+ 1 jaune d’oeuf)
180 g de poudre d’amande
75 g de beurre doux (bien mou)
100 g de sucre de canne complet
1 pincée de vanille en poudre
2 càs de lait ou boisson végétale
Ne pas oublier… la fève :) !
PRÉPARATION
1/ Préchauffez le four à 180°C. Mélangez le beurre bien mou (réchauffez si besoin) avec le sucre à l’aide d’une spatule. Ajoutez la poudre d’amande, le rhum, les grains de vanille et les 2 œufs battus en omelette. Mélangez le tout.

2/ Disposez la pâte feuilletée et déposez-y la préparation en laissant 2 cm de pâte tout autour. Placez la fève à l’endroit de votre choix.

3/ Mouillez légèrement les contours de la pâte puis recouvrez avec la seconde pâte feuilletée. Soudez bien les bords avec les doigts.

4/ Cassez l’œuf restant et prélevez le jaune : mélangez-le avec le lait, puis badigeonnez la pâte feuilletée à l’aide d’un pinceau ou d’une fourchette. 6/ Dessinez sur la pâte des motifs avec une lame de couteau. Swipe à droite pour voir le motif que j’ai utilisé :)

7/ Faites cuire à 180°C pendant 25 à 30 minutes. Dégustez chaud ou tiède :).",
"http://localhost:8080/galettedesrois.png", '2022-10-12T12:00:00', 0, 3, 1),
("Asperges grillées au four, à l’huile d’olive, à l’ail et au citron (recette facile⭐)",
"INGRÉDIENTS
1/2 botte d'asperges vertes
2 càs d'huile d'olive extra-vierge
1/2 citron jaune
1 gousse d'ail
Du sel

PRÉPARATION
1/ Préchauffe ton four à 180°C. Lave tes asperges et coupe les extrémités blanches (parties les plus dures et peu digestes). Si besoin, rape les queues des asperges à l'aide d'un économe. 
2/ Dispose les asperges dans un plat allant au four. Dans un bol, mélange l'huile d'olive, le jus de citron, l'ail et le sel.
Verse le tout sur les asperges, puis enfourne 15-25 minutes selon l'épaisseur des asperges. Servir chaud. Bon appétit !",
"http://localhost:8080/asperge.png", '2023-10-31T14:35:06', 0, 3, 2),
("Salade d’automne végétarienne (recette facile⭐)",
"INGRÉDIENTS
1 petite courge butternut
120 g de pois chiches cuits, en conserve ou faits-maison
100 g de chou kale
1/2 brocoli
Sel & Poivre
1 c. à café de curry (ou autres épices au choix)
4 c. à soupe de yaourt à la grecque
1 petite gousse d'ail
1 c. à soupe de jus de citron
1 c. à café de moutarde
1 c. à soupe d'huile d'olive
1 poignée de persil frais
Sel et poivre.

PRÉPARATION
1/ Préchauffez votre four à 180°C. Epluchez la courge butternut puis coupez la chair en dés de taille moyenne. Disposez sur une plaque de cuisson puis enfournez 45 minutes environ, en veillant à retourner les dés de courge à mi-cuisson. (Pour sauter cette étape, pensez à toujours avoir dans votre frigo des restes de courge !)
2/ Dans un saladier, ajoutez les pois chiches cuits, 1 cuillère à soupe d'huile d'olive et le curry. Mélangez. Versez le tout sur une autre plaque de cuisson et faites cuire 15 minutes environ (jusqu'à que les pois chiches soient joliment dorés). Une fois cuits, laissez refroidir.
3/ Pendant ce temps, faites bouillir une casserole d'eau. Lavez puis détachez les têtes de brocoli. Plongez-les dans l'eau bouillante légèrement salée et faites cuire 10 minutes. Afin de préserver au mieux les bienfaits du brocoli, ce dernier doit rester légèrement ferme. Une fois cuits, passez les fleurs de brocoli sous l'eau froide et laissez refroidir.
4/ Coupez les extrémités dures du chou kale à l'aide d'un couteau puis coupez les plus grosses feuilles en petits morceaux. Faites tremper le tout 10 minutes dans de l'eau froide afin que les feuilles ramollissent et soient plus digestes.
5/ Préparez la vinaigrette : pour cela, mélangez le yaourt à la grecque, l'ail finement haché, le jus de citron, l'huile d'olive et le persil haché. Salez et poivrez.
6/ Dans un grand saladier, ajoutez tous les ingrédients. Versez la vinaigrette puis mélangez soigneusement le tout. Rectifiez si besoin l'assaisonnement puis... régalez-vous ! Bon appétit!",
"http://localhost:8080/saladeautomne.png", '2024-06-26T16:45:56', 0, 3, 1);

INSERT INTO comment (content, date, id_article) VALUES
("Super article", '2024-03-14 18:15:00', 1),
("Super article", '2024-03-14 18:15:00', 1),
("Super article", '2024-03-14 18:15:00', 1),
("Super article", '2024-03-14 18:15:00', 1),
("Super article", '2024-03-14 18:15:00', 1),
("Super article", '2024-03-14 18:15:00', 1),
("Super article", '2024-03-14 18:15:00', 1),
("Super article", '2024-03-14 18:15:00', 1);




-- SELECT * FROM article WHERE id_categorie=1;
-- SELECT * FROM article WHERE id_categorie=2;
-- SELECT * FROM article WHERE id_categorie=3;

SELECT * FROM article ;
SELECT * FROM article WHERE id_categorie=1 ORDER BY id DESC LIMIT 3 ;
SELECT * FROM article WHERE id_categorie=2 ORDER BY id DESC LIMIT 3 ;
SELECT * FROM article WHERE id_categorie=3 ORDER BY id DESC LIMIT 3 ;


SELECT * FROM article WHERE id_user=1 ORDER BY id DESC LIMIT 3 ;
-- UPDATE article SET title="test", content="test", picture="test", date='2024-03-14 18:15:00', vue=3, author="nono", id_categorie=3 WHERE id=1;
